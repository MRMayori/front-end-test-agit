Berikut saya lampirkan tugas Front End yang telah diberikan pada hari senin lalu, 
Terdapat beberapa menu yaitu:
1. Menu List Customer, Halaman ini berisikan daftar Customer yang telah melakukan transaksi. Dalam Halaman ini menampilkan detail informasi dalam customer yaitu Nama Customer, Status Customer, Gender dan Email. Data data tersebut telah disimpan pada API sehingga dapat ditampilkan dalam halaman ini
2. Menu Detail Customer, Halaman ini berisikan Detail Customer. Dalam Halaman ini menampilkan detail informasi dalam customer yaitu Nama Customer, Status Customer, Gender dan Email seperti pada halaman 1.

Mohon maaf apabila masih terdapat kekurangan dalam pengerjaan.